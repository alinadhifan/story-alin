# Repository Story 7 - 9 | PPW

[![pipeline status](https://gitlab.com/alinadhifan/story-alin/badges/master/pipeline.svg)](https://gitlab.com/alinadhifan/story-alin/-/commits/master)
[![coverage report](https://gitlab.com/alinadhifan/story-alin/badges/master/coverage.svg)](https://gitlab.com/alinadhifan/story-alin/-/commits/master)

```bash
Name        : Alina Dhifan Ajriya
NPM         : 1906399474
Class       : PPW-C
```

## Link Website Herokuapp

Story 7
[https://story-alin.herokuapp.com/accordion/](https://story-alin.herokuapp.com/accordion/)


Story 8
[https://story-alin.herokuapp.com/books/](https://story-alin.herokuapp.com/books/)

Story 9
[https://story-alin.herokuapp.com/books/](https://story-alin.herokuapp.com/login/)