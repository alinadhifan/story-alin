from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from . import views

class Story7TestCase(TestCase):
    def test_root_url_status_200(self):
        response = Client().get(reverse('story7:index'))
        self.assertEqual(response.status_code, 200)

    def test_template_home(self):
        response = Client().get(reverse('story7:index'))
        self.assertTemplateUsed(response, 'index.html')

    def test_func_home(self):
        found = resolve('/accordion/')
        self.assertEqual(found.func, views.index)



# class MainFunctionalTestCase(FunctionalTestCase):
#     def test_root_url_exists(self):
#         self.selenium.get(f'{self.live_server_url}/')
#         html = self.selenium.find_element_by_tag_name('html')
#         self.assertNotIn('not found', html.text.lower())
#         self.assertNotIn('error', html.text.lower())
